import{j as e}from"./jsx-runtime-D_3pcrNB.js";import{a as g,b as p,R as h,c as b,d as n,e as o}from"./rmg-layout-DdyA7PP2.js";import{R as x}from"./rmg-debounced-input-CNI6z-W-.js";import{R as v}from"./rmg-label-cSd5ZoT_.js";import{B as i}from"./box-DK0ghdlA.js";import{f as q,a as R,c as f}from"./use-style-config-Be-hB2Uk.js";import{o as j,c as H}from"./theming-props-CtumPZyV.js";import"./index-YTOF1_EJ.js";import"./flex-BEESXl3V.js";import"./input-bB7IpTGB.js";import"./use-form-control-DBMiy26T.js";import"./context-CXQEnuw_.js";import"./use-merge-refs-CDN4nSPx.js";import"./attr-sHTFgF0E.js";import"./call-all-DDzecCnU.js";import"./icon-BmjnIw9M.js";const a=q(function(s,d){const l=R("Heading",s),{className:P,...c}=j(s);return e.jsx(f.h2,{ref:d,className:H("chakra-heading",s.className),...c,__css:l})});a.displayName="Heading";const Q={title:"RmgLayout"},t=()=>e.jsx(i,{h:400,w:700,background:"var(--chakra-colors-chakra-body-bg)",children:e.jsxs(g,{border:"1px dashed",children:[e.jsx(p,{border:"1px dashed",children:e.jsx(a,{as:"h4",size:"md",children:"RMG Components"})}),e.jsxs(h,{border:"1px dashed",children:[e.jsx(b,{border:"1px dashed",children:e.jsx(v,{label:"Quick filter",children:e.jsx(x,{placeholder:"Filter anything"})})}),e.jsxs(i,{overflowY:"scroll",background:"inherit",children:[e.jsxs(n,{children:[e.jsx(o,{children:e.jsx(a,{as:"h5",size:"sm",children:"Section 1"})}),e.jsx(i,{children:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."})]}),e.jsxs(n,{children:[e.jsx(o,{children:e.jsx(a,{as:"h5",size:"sm",children:"Section 2"})}),e.jsx(i,{children:"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam elementum porttitor tincidunt. In vel dui non odio tempus luctus sed sit amet orci. Nam imperdiet euismod tempor. Maecenas non imperdiet dui. Vestibulum iaculis ex arcu, ut rutrum leo aliquam sit amet. Morbi porttitor dignissim nulla, vitae molestie ante efficitur non. Pellentesque quis hendrerit metus, ac sagittis neque. Morbi ac odio quis velit venenatis dictum in sit amet augue. Vestibulum ut metus urna. Etiam ex dui, volutpat non tortor vitae, aliquet sodales augue. Pellentesque at orci mattis, interdum eros in, tempus quam. Curabitur imperdiet sed dui id mollis. Praesent tincidunt orci ut venenatis rutrum. Vivamus bibendum orci eu ante pretium, vel commodo odio accumsan. Duis magna ante, finibus vel tincidunt et, dignissim a nisi. Maecenas congue lectus lorem, a maximus mi pellentesque vel. Suspendisse lobortis eu leo et consequat. Ut lobortis tellus erat, rutrum suscipit tellus posuere non. Vivamus malesuada faucibus vestibulum. Mauris quis lectus at justo interdum interdum. Nullam imperdiet enim felis, at sagittis est sagittis a. Nam sed gravida est, ac consectetur dolor. Sed vel neque vel turpis condimentum eleifend. Quisque vitae consequat quam. Nullam egestas molestie dolor at porta. Integer eget nulla sit amet odio commodo pharetra. Fusce vel iaculis nunc, at pharetra erat."})]})]})]})]})});var r,u,m;t.parameters={...t.parameters,docs:{...(r=t.parameters)==null?void 0:r.docs,source:{originalSource:`() => {
  return <Box h={400} w={700} background="var(--chakra-colors-chakra-body-bg)">
            <RmgWindow border="1px dashed">
                <RmgWindowHeader border="1px dashed">
                    <Heading as="h4" size="md">
                        RMG Components
                    </Heading>
                </RmgWindowHeader>

                <RmgPage border="1px dashed">
                    <RmgPageHeader border="1px dashed">
                        <RmgLabel label="Quick filter">
                            <RmgDebouncedInput placeholder="Filter anything" />
                        </RmgLabel>
                    </RmgPageHeader>

                    <Box overflowY="scroll" background="inherit">
                        <RmgSection>
                            <RmgSectionHeader>
                                <Heading as="h5" size="sm">
                                    Section 1
                                </Heading>
                            </RmgSectionHeader>
                            <Box>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                                exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                            </Box>
                        </RmgSection>

                        <RmgSection>
                            <RmgSectionHeader>
                                <Heading as="h5" size="sm">
                                    Section 2
                                </Heading>
                            </RmgSectionHeader>
                            <Box>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam elementum porttitor
                                tincidunt. In vel dui non odio tempus luctus sed sit amet orci. Nam imperdiet euismod
                                tempor. Maecenas non imperdiet dui. Vestibulum iaculis ex arcu, ut rutrum leo aliquam
                                sit amet. Morbi porttitor dignissim nulla, vitae molestie ante efficitur non.
                                Pellentesque quis hendrerit metus, ac sagittis neque. Morbi ac odio quis velit venenatis
                                dictum in sit amet augue. Vestibulum ut metus urna. Etiam ex dui, volutpat non tortor
                                vitae, aliquet sodales augue. Pellentesque at orci mattis, interdum eros in, tempus
                                quam. Curabitur imperdiet sed dui id mollis. Praesent tincidunt orci ut venenatis
                                rutrum. Vivamus bibendum orci eu ante pretium, vel commodo odio accumsan. Duis magna
                                ante, finibus vel tincidunt et, dignissim a nisi. Maecenas congue lectus lorem, a
                                maximus mi pellentesque vel. Suspendisse lobortis eu leo et consequat. Ut lobortis
                                tellus erat, rutrum suscipit tellus posuere non. Vivamus malesuada faucibus vestibulum.
                                Mauris quis lectus at justo interdum interdum. Nullam imperdiet enim felis, at sagittis
                                est sagittis a. Nam sed gravida est, ac consectetur dolor. Sed vel neque vel turpis
                                condimentum eleifend. Quisque vitae consequat quam. Nullam egestas molestie dolor at
                                porta. Integer eget nulla sit amet odio commodo pharetra. Fusce vel iaculis nunc, at
                                pharetra erat.
                            </Box>
                        </RmgSection>
                    </Box>
                </RmgPage>
            </RmgWindow>
        </Box>;
}`,...(m=(u=t.parameters)==null?void 0:u.docs)==null?void 0:m.source}}};const U=["Basic"];export{t as Basic,U as __namedExportsOrder,Q as default};
