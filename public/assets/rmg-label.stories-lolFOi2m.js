import{j as e}from"./jsx-runtime-D_3pcrNB.js";import{R as t}from"./rmg-label-cSd5ZoT_.js";import{R as a}from"./rmg-debounced-input-CNI6z-W-.js";import{V as o}from"./v-stack-Bwg73ych.js";import"./index-YTOF1_EJ.js";import"./use-style-config-Be-hB2Uk.js";import"./use-form-control-DBMiy26T.js";import"./context-CXQEnuw_.js";import"./theming-props-CtumPZyV.js";import"./use-merge-refs-CDN4nSPx.js";import"./attr-sHTFgF0E.js";import"./call-all-DDzecCnU.js";import"./icon-BmjnIw9M.js";import"./input-bB7IpTGB.js";import"./stack-D8gPABXB.js";import"./children-DxTy7EQl.js";const w={title:"RmgLabel",component:t},r=()=>e.jsxs(o,{children:[e.jsx(t,{label:"Basic input",children:e.jsx(a,{})}),e.jsx(t,{label:"Basic input",helper:"Input with helper text",children:e.jsx(a,{})}),e.jsx(t,{label:"Basic input",errorMessage:"Input with error message",children:e.jsx(a,{isInvalid:!0})})]});var n,s,i;r.parameters={...r.parameters,docs:{...(n=r.parameters)==null?void 0:n.docs,source:{originalSource:`() => {
  return <VStack>
            <RmgLabel label="Basic input">
                <RmgDebouncedInput />
            </RmgLabel>

            <RmgLabel label="Basic input" helper="Input with helper text">
                <RmgDebouncedInput />
            </RmgLabel>

            <RmgLabel label="Basic input" errorMessage="Input with error message">
                <RmgDebouncedInput isInvalid={true} />
            </RmgLabel>
        </VStack>;
}`,...(i=(s=r.parameters)==null?void 0:s.docs)==null?void 0:i.source}}};const D=["Basic"];export{r as Basic,D as __namedExportsOrder,w as default};
