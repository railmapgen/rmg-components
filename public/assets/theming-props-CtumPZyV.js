import{o as i}from"./use-style-config-Be-hB2Uk.js";const e=(...o)=>o.filter(Boolean).join(" ");function n(o){return i(o,["styleConfig","size","variant","colorScheme"])}export{e as c,n as o};
