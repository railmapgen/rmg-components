function a(...c){return function(...l){c.forEach(r=>r==null?void 0:r(...l))}}function o(...c){return function(l){c.some(r=>(r==null||r(l),l==null?void 0:l.defaultPrevented))}}export{a,o as c};
