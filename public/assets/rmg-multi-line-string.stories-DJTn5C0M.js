import{j as o}from"./jsx-runtime-D_3pcrNB.js";import{R as n}from"./rmg-multi-line-string-BOeXWy7V.js";import"./index-YTOF1_EJ.js";import"./use-style-config-Be-hB2Uk.js";import"./theming-props-CtumPZyV.js";const d={title:"RmgMultiLineString",component:n},t=()=>o.jsx(n,{text:`First line
Second line
Third line`,delimiter:`
`});var e,r,i;t.parameters={...t.parameters,docs:{...(e=t.parameters)==null?void 0:e.docs,source:{originalSource:`() => {
  const text = 'First line\\nSecond line\\nThird line';
  return <RmgMultiLineString text={text} delimiter={'\\n'} />;
}`,...(i=(r=t.parameters)==null?void 0:r.docs)==null?void 0:i.source}}};const u=["Basic"];export{t as Basic,u as __namedExportsOrder,d as default};
